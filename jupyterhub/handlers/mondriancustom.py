from .base import BaseHandler

from .. import orm
from ..utils import maybe_future, url_path_join

class MondrianCustomHandler(BaseHandler):
    async def create_user_from_nativeauthenticator(self, username, pw, user_real_name, email, phone, organization, has_2fa):
        """Login a user"""
        print('MondrianCustomHandler create_user_from_nativeauthenticator()')
        username = username
        auth_state = None
        admin = 0
        new_user = username not in self.users
        user = self.user_from_username(username)
        await maybe_future(self.authenticator.add_user(user))
        self.db.commit()

    async def set_jupyterhub_user_admin(self, username):
        user = orm.User.find(self.db, username)
        user.admin = not user.admin
        self.db.commit()

    def get_jupyterhub_users(self):
        available = {'name', 'admin', 'running', 'last_activity'}
        default_sort = ['admin', 'name']
        mapping = {
            'running': orm.Spawner.server_id,
        }
        for name in available:
            if name not in mapping:
                mapping[name] = getattr(orm.User, name)

        default_order = {
            'name': 'asc',
            'last_activity': 'desc',
            'admin': 'desc',
            'running': 'desc',
        }

        sorts = self.get_arguments('sort') or default_sort
        orders = self.get_arguments('order')

        for bad in set(sorts).difference(available):
            self.log.warning("ignoring invalid sort: %r", bad)
            sorts.remove(bad)
        for bad in set(orders).difference({'asc', 'desc'}):
            self.log.warning("ignoring invalid order: %r", bad)
            orders.remove(bad)

        # add default sort as secondary
        for s in default_sort:
            if s not in sorts:
                sorts.append(s)
        if len(orders) < len(sorts):
            for col in sorts[len(orders):]:
                orders.append(default_order[col])
        else:
            orders = orders[:len(sorts)]

        # this could be one incomprehensible nested list comprehension
        # get User columns
        cols = [ mapping[c] for c in sorts ]
        # get User.col.desc() order objects
        ordered = [ getattr(c, o)() for c, o in zip(cols, orders) ]

        users = self.db.query(orm.User).outerjoin(orm.Spawner).order_by(*ordered)
        users = [ self._user_from_orm(u) for u in users ]
        return users
