import pytz
from datetime import datetime

def tryGettingKey(dict, key):
    try:
        return dict[key]
    except KeyError:
        return None

def convertFloat(str, places, multiple):
    return format(float(str)*multiple, '.'+places+'f')

def formatBytes(sizeStr):
    size = int(sizeStr)
    # 2**10 = 1024
    power = 2**10
    n = 0
    power_labels = {0 : '', 1: 'K', 2: 'M', 3: 'G', 4: 'T'}
    while size > power:
        size /= power
        n += 1
    return format(size, '.2f')+power_labels[n]+'B'

#  Time

def getCurrentTimeKoreaSeoul():
#     fmt = "%Y-%m-%d %H:%M:%S %Z%z"
    fmt = "%Y-%m-%d %H:%M:%S"
    return datetime.now(pytz.timezone('Asia/Seoul')).strftime(fmt)

def convertDateTimeToKoreaSeoul(utc_time):
#     fmt = "%Y-%m-%d %H:%M:%S %Z%z"
    fmt = "%Y-%m-%d %H:%M:%S"
    return pytz.utc.localize(utc_time, is_dst=None).astimezone(pytz.timezone('Asia/Seoul')).strftime(fmt)
