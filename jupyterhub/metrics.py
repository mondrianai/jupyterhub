"""
Prometheus metrics exported by JupyterHub

Read https://prometheus.io/docs/practices/naming/ for naming
conventions for metrics & labels. We generally prefer naming them
`<noun>_<verb>_<type_suffix>`. So a histogram that's tracking
the duration (in seconds) of servers spawning would be called
SERVER_SPAWN_DURATION_SECONDS.

We also create an Enum for each 'status' type label in every metric
we collect. This is to make sure that the metrics exist regardless
of the condition happening or not. For example, if we don't explicitly
create them, the metric spawn_duration_seconds{status="failure"}
will not actually exist until the first failure. This makes dashboarding
and alerting difficult, so we explicitly list statuses and create
them manually here.
"""
from enum import Enum

from prometheus_client import Histogram

try:
    from kubernetes import client, config
    from kubernetes.client import ApiClient
except ImportError as e:
    print('error while importing kubernetes', e)

import time
import json
import requests

REQUEST_DURATION_SECONDS = Histogram(
    'request_duration_seconds',
    'request duration for all HTTP requests',
    ['method', 'handler', 'code']
)

SERVER_SPAWN_DURATION_SECONDS = Histogram(
    'server_spawn_duration_seconds',
    'time taken for server spawning operation',
    ['status'],
    # Use custom bucket sizes, since the default bucket ranges
    # are meant for quick running processes. Spawns can take a while!
    buckets=[0.5, 1, 2.5, 5, 10, 15, 30, 60, 120, float("inf")]
)

class ServerSpawnStatus(Enum):
    """
    Possible values for 'status' label of SERVER_SPAWN_DURATION_SECONDS
    """
    success = 'success'
    failure = 'failure'
    already_pending = 'already-pending'
    throttled = 'throttled'
    too_many_users = 'too-many-users'

    def __str__(self):
        return self.value

for s in ServerSpawnStatus:
    # Create empty metrics with the given status
    SERVER_SPAWN_DURATION_SECONDS.labels(status=s)


PROXY_ADD_DURATION_SECONDS = Histogram(
    'proxy_add_duration_seconds',
    'duration for adding user routes to proxy',
    ['status']
)

class ProxyAddStatus(Enum):
    """
    Possible values for 'status' label of PROXY_ADD_DURATION_SECONDS
    """
    success = 'success'
    failure = 'failure'

    def __str__(self):
        return self.value

for s in ProxyAddStatus:
    PROXY_ADD_DURATION_SECONDS.labels(status=s)

def prometheus_log_method(handler):
    """
    Tornado log handler for recording RED metrics.

    We record the following metrics:
       Rate – the number of requests, per second, your services are serving.
       Errors – the number of failed requests per second.
       Duration – The amount of time each request takes expressed as a time interval.

    We use a fully qualified name of the handler as a label,
    rather than every url path to reduce cardinality.

    This function should be either the value of or called from a function
    that is the 'log_function' tornado setting. This makes it get called
    at the end of every request, allowing us to record the metrics we need.
    """
    REQUEST_DURATION_SECONDS.labels(
        method=handler.request.method,
        handler='{}.{}'.format(handler.__class__.__module__, type(handler).__name__),
        code=handler.get_status()
    ).observe(handler.request.request_time())


class PrometheusMetrics():
    def __init__(self):
        try:
            config.load_incluster_config()
            self.api_client = ApiClient()
        except Exception as e:
            print('exception while initializing prometheus metrics class')
            print(e)

    def getNodeCpuUsage(self, nodeName):
        now = str(int(time.time()))
        try:
            response = self.api_client.call_api('/api/v1/namespaces/monitoring/services/prometheus-k8s:9090/proxy/api/v1/query?query=instance%3Anode_cpu_utilisation%3Arate1m%7Bjob%3D%22node-exporter%22%2C%20instance%3D%22' + nodeName + '%22%7D&time=' + now,'GET', auth_settings = ['BearerToken'], response_type='json', _preload_content=False)
            print("getNodeCpuUsage", response)
            jsonStr = response[0].data.decode('utf-8')
            print("getNodeCpuUsage", jsonStr)

            try:
                jsonData = json.loads(jsonStr)
                cpuUsage = jsonData["data"]["result"]
                if len(cpuUsage) < 1:
                    return None
            except Exception as e:
                print('Exception while getting node cpuUsage in response', e)
                return None

            return cpuUsage[0]["value"][1]
        except Exception as e:
            print('exception while querying node cpu usgae')
            print(e)
            return None

    def getNodeMemoryUsage(self, nodeName):
        now = str(int(time.time()))
        try:
            response = self.api_client.call_api('/api/v1/namespaces/monitoring/services/prometheus-k8s:9090/proxy/api/v1/query?query=instance%3Anode_memory_utilisation%3Aratio%7Bjob%3D%22node-exporter%22%2C%20job%3D%22node-exporter%22%2C%20instance%3D%22' + nodeName + '%22%7D&time=' + now,'GET', auth_settings = ['BearerToken'], response_type='json', _preload_content=False)
            print("getNodeMemoryUsage", response)
            jsonStr = response[0].data.decode('utf-8')
            print("getNodeMemoryUsage", jsonStr)

            try:
                jsonData = json.loads(jsonStr)
                memoryUsage = jsonData["data"]["result"]
                if len(memoryUsage) < 1:
                    return None
            except Exception as e:
                print('Exception while getting node memoryUsage in response', e)
                return None

            return memoryUsage[0]["value"][1]
        except Exception as e:
            print('exception while querying node memory usgae')
            print(e)
            return None

    # def getNodeDiskUsage(self, nodeName):

    # def getCephDiskUsage(self):


    def getPodCpuUsage(self, namespace, podName):
        now = str(int(time.time()))
        try:
            response = self.api_client.call_api('/api/v1/namespaces/monitoring/services/prometheus-k8s:9090/proxy/api/v1/query?query=sum(namespace_pod_container%3Acontainer_cpu_usage_seconds_total%3Asum_rate%7Bnamespace%3D%22' + namespace + '%22%2C%20pod%3D%22'+ podName +'%22%2C%20container!%3D%22POD%22%2C%20cluster%3D%22%22%7D)%20by%20(container)&time=' + now,'GET', auth_settings = ['BearerToken'], response_type='json', _preload_content=False)
            print("getPodCpuUsage", response)
            jsonStr = response[0].data.decode('utf-8')
            print("getPodCpuUsage", jsonStr)

            try:
                jsonData = json.loads(jsonStr)
                cpuUsage = jsonData["data"]["result"]
                if len(cpuUsage) < 1:
                    return None
            except Exception as e:
                print('Exception while getting pod cpuUsage in response', e)
                return None

            return cpuUsage[0]["value"][1]
        except Exception as e:
            print('exception while querying pod cpu usgae')
            print(e)
            return None

    def getPodMemoryUsage(self, namespace, podName):
        now = str(int(time.time()))
        try:
            response = self.api_client.call_api('/api/v1/namespaces/monitoring/services/prometheus-k8s:9090/proxy/api/v1/query?query=sum(container_memory_rss%7Bcluster%3D%22%22%2C%20namespace%3D%22'+ namespace +'%22%2C%20pod%3D%22'+ podName +'%22%2C%20container!%3D%22POD%22%2C%20container!%3D%22%22%7D)%20by%20(container)&time=' + now,'GET', auth_settings = ['BearerToken'], response_type='json', _preload_content=False)
            print("getPodMemoryUsage", response)
            jsonStr = response[0].data.decode('utf-8')
            print("getPodMemoryUsage", jsonStr)

            try:
                jsonData = json.loads(jsonStr)
                memoryUsage = jsonData["data"]["result"]
                if len(memoryUsage) < 1:
                    return None
            except Exception as e:
                print('Exception while getting pod memoryUsage in response', e)
                return None

            return memoryUsage[0]["value"][1]
        except Exception as e:
            print('exception while querying pod memory usgae')
            print(e)
            return None

    # def getPvcDiskUsage(self, namespace, pvcName):
    #     now = str(int(time.time()))
    #     try:
    #         response = self.api_client.call_api('/api/v1/namespaces/monitoring/services/prometheus-k8s:9090/proxy/api/v1/query?query=(%0A%20%20sum%20without(instance%2C%20node)%20(kubelet_volume_stats_capacity_bytes%7Bcluster%3D%22%22%2C%20job%3D%22kubelet%22%2C%20namespace%3D%22'+namespace+'%22%2C%20persistentvolumeclaim%3D%22'+pvcName+'%22%7D)%0A%20%20-%0A%20%20sum%20without(instance%2C%20node)%20(kubelet_volume_stats_available_bytes%7Bcluster%3D%22%22%2C%20job%3D%22kubelet%22%2C%20namespace%3D%22'+ namespace +'%22%2C%20persistentvolumeclaim%3D%22'+ pvcName +'%22%7D)%0A)%0A&time=' + now,'GET', auth_settings = ['BearerToken'], response_type='json', _preload_content=False)
    #         print("getPvcDiskUsage", response)
    #         metrics_data = response[0].data.decode('utf-8')
    #         print("getPvcDiskUsage", metrics_data)
    #         return metrics_data
    #     except Exception as e:
    #         print('exception while querying pvc disk usgae')
    #         print(e)
    #         return None

    def getPvcsByNamespace(self, namespace):
        now = str(int(time.time()))
        try:
            response = self.api_client.call_api('/api/v1/namespaces/monitoring/services/prometheus-k8s:9090/proxy/api/v1/query?query=(%0A%20%20sum%20without(instance%2C%20node)%20(kubelet_volume_stats_capacity_bytes%7Bcluster%3D""%2C%20job%3D"kubelet"%2C%20namespace%3D"'+namespace+'"%7D)%0A%20%20-%0A%20%20sum%20without(instance%2C%20node)%20(kubelet_volume_stats_available_bytes%7Bcluster%3D""%2C%20job%3D"kubelet"%2C%20namespace%3D"'+namespace+'"%7D)%0A)%0A&time=' + now,'GET', auth_settings = ['BearerToken'], response_type='json', _preload_content=False)
            print("getPvcsByNamespace", response)
            jsonStr = response[0].data.decode('utf-8')
            print("getPvcsByNamespace", jsonStr)
            try:
                jsonData = json.loads(jsonStr)
                wrappedPvcs = jsonData["data"]["result"]
                if len(wrappedPvcs) < 1:
                    return {}
            except Exception as e:
                print('Exception while getting pvc infos in response', e)
                return {}
            
            pvcInfos = list(map(lambda wrappedPvc: { "pvcName": wrappedPvc["metric"]["persistentvolumeclaim"], "diskUsage": wrappedPvc["value"][1] }, wrappedPvcs))
            pvcInfosMap = {}
            for pvcInfo in pvcInfos:
                pvcInfosMap[pvcInfo["pvcName"]] = pvcInfo
            return pvcInfosMap
        except Exception as e:
            print('exception while querying pvc infos')
            print(e)
            return {}

    def getNodeNames(self):
        now = str(int(time.time()))
        try:
            response = self.api_client.call_api('/api/v1/namespaces/monitoring/services/prometheus-k8s:9090/proxy/api/v1/query?query=kubelet_node_name&time=' + now,'GET', auth_settings = ['BearerToken'], response_type='json', _preload_content=False)
            print("getNodeNames", response)
            jsonStr = response[0].data.decode('utf-8')
            print("getNodeNames", jsonStr)
            try:
                jsonData = json.loads(jsonStr)
                wrappedNodes = jsonData["data"]["result"]
                if len(wrappedNodes) < 1:
                    return []
            except Exception as e:
                print('Exception while getting nodes in response', e)
                return []
            
            nodeNames = list(map(lambda wrappedNode: wrappedNode["metric"]["node"], wrappedNodes))

            return nodeNames
        except Exception as e:
            print('exception while querying node names')
            print(e)
            return []
    
    def getPodNames(self, namespace):
        now = str(int(time.time()))
        try:
            response = self.api_client.call_api('/api/v1/namespaces/monitoring/services/prometheus-k8s:9090/proxy/api/v1/query?query=kube_pod_info%7Bnamespace%3D%22'+namespace+'%22%7D&time=' + now,'GET', auth_settings = ['BearerToken'], response_type='json', _preload_content=False)
            print("getPodNames", response)
            jsonStr = response[0].data.decode('utf-8')
            print("getPodNames", jsonStr)
            try:
                jsonData = json.loads(jsonStr)
                wrappedPods = jsonData["data"]["result"]
                if len(wrappedPods) < 1:
                    return []
            except Exception as e:
                print('Exception while getting nodes in response', e)
                return []
            
            podNames = list(map(lambda wrappedPod: { "namespace": namespace, "podName": wrappedPod["metric"]["pod"] }, wrappedPods))

            return podNames
        except Exception as e:
            print('exception while querying node names')
            print(e)
            return []
    def getCephUsageMetrics(self):
        r = requests.get('http://ceph-api-service')
        res = r.json()
        usage = str(int(res["kb_used"])/int(res["kb"]))
        return usage
    
    def getNodeMetrics(self, nodeName):
        node = {
            "nodeName": nodeName,
            "cpuUsage": self.getNodeCpuUsage(nodeName),
            "memoryUsage": self.getNodeMemoryUsage(nodeName),
            "diskUsage": self.getCephUsageMetrics(),
        }
        return node

    def getPodMetrics(self, podInfo):
        namespace = podInfo["namespace"]
        podName = podInfo["podName"]
        pod = {
            "namespace": namespace,
            "podName": podName,
            "cpuUsage": self.getPodCpuUsage(namespace, podName),
            "memoryUsage": self.getPodMemoryUsage(namespace, podName),
            "diskUsage": None,
        }
        return pod

    def getNodesMetricsList(self):
        #get list of nodes
        nodeNames = self.getNodeNames()
        return list(map(self.getNodeMetrics, nodeNames))

    def getPodsMetricsMap(self):
        #get list of pods
        podNames = self.getPodNames("jhub")
        podsMetrics = list(map(self.getPodMetrics, podNames))

        #get pvcs
        pvcInfosMap = self.getPvcsByNamespace("jhub")

        for podMetrics in podsMetrics:
            if "jupyter-" in podMetrics["podName"]:
                try:
                    pvcInfo = pvcInfosMap[podMetrics["podName"].replace("jupyter-", "claim-")]
                except KeyError:
                    continue

                podMetrics["diskUsage"] = pvcInfo["diskUsage"]
            else:
                continue

        podsMetricsMap = {}

        for podMetrics in podsMetrics:
            podsMetricsMap[podMetrics["podName"].replace("jupyter-", "")] = podMetrics

        return podsMetricsMap


prometheusMetrics = PrometheusMetrics()
