// Copyright (c) Jupyter Development Team.
// Distributed under the terms of the Modified BSD License.

require(["jquery", "bootstrap", "moment", "jhapi", "sweetalert2"], function ($, bs, moment, JHAPI, Swal ) {
    "use strict";

    var base_url = window.jhdata.base_url;
    var prefix = window.jhdata.prefix;
    var admin_access = window.jhdata.admin_access;
    var options_form = window.jhdata.options_form;

    const api = new JHAPI(base_url);

    function funcForAuthorization () {
        const url = window.location.pathname;
        const id = url.substring(url.lastIndexOf('/') + 1);
        window.location.replace(`/authorize/${id}`);
    }

    function funcForSetAdmin () {
        const url = window.location.pathname;
        const id = url.substring(url.lastIndexOf('/') + 1);
        window.location.replace(`/change-admin/${id}`);
    }

    $("#btn--authorize").click(function () {
        funcForAuthorization();
    });

    $("#btn--unauthorize").click(function () {
        funcForAuthorization();
    });

    $("#btn--set-not-admin").click(function () {
        funcForSetAdmin();
    });

    $("#btn--set-admin").click(function () {
        funcForSetAdmin();
    });

    $("#btn--Authorize-own").click(function () {
        Swal.fire(
          'Authorized',
          'You are already authorized user.',
          'success'
        )
    });

    $("#btn--admin-own").click(function () {
        Swal.fire(
          'Admin',
          'You are already a system admin.',
          'success'
        )
    });

    $("#btn--delete-user-own").click(function () {
        Swal.fire({
            type: 'error',
            title: 'Sorry',
            text: 'You don\'t delete your own account.',
        });
    });

    $('#btn--delete-user').click(function () {
        Swal.fire({
            title: 'Are you sure?',
            text: "Once you delete the account, there's no getting it back. Make sure you want to do this.",
            input: 'text',
            type: 'warning',
            inputAttributes: {
                autocapitalize: 'off'
            },
            inputPlaceholder: 'Confirm by typing DELETE',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            showLoaderOnConfirm: true,
            allowOutsideClick: () => !Swal.isLoading(),
            preConfirm: (typing) => {
                if (typing && typing === 'DELETE') {
                    return typing;
                } else {
                    Swal.showValidationMessage(
                      `If you want to delete the account, type'DELETE' and press the delete button.`
                    )
                }
            },
        }).then((result) => {
            if(result.value === 'DELETE') {
                const url = window.location.pathname;
                const id = url.substring(url.lastIndexOf('/') + 1);
                api.delete_user(id, {
                    success: function () {
                        window.location.replace('/authorize');
                    }
                });
            }
        });
    });

    $("#btn-toast").click(function () {
        $("#toast-change-information").addClass('d-none');
    });

});

